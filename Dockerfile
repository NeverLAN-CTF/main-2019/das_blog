FROM neverlanctf/base:4th-year

MAINTAINER Zane Durkin <zane@neverlanctf.org>

COPY web/ /var/www/html/
# copy db file
COPY tmp/ /tmp/
# create mysql password for root and set to /tmp/pass_root file
RUN \
    export MYSQL_PASS=$(openssl rand -hex 100) && \
    echo $MYSQL_PASS > /tmp/pass_root && \
    export MYSQL_PASS=$(openssl rand -hex 100) && \
    echo $MYSQL_PASS > /tmp/pass_web && \
    unset MYSQL_PASS

# set up php file with password
RUN sed -i 's/<password_web>/'$(cat /tmp/pass_web)'/g' /var/www/html/inc/db_init.php
# finally edit db.sql file with passwords
RUN \
    sed -i 's/<password_web>/'$(cat /tmp/pass_web)'/g' /tmp/db.sql && \
    sed -i 's/<password_root>/'$(cat /tmp/pass_root)'/g' /tmp/db.sql

# this file will be needed for updating 
COPY entrypoint.sh /etc/entrypoint.sh
## setup database
CMD /etc/entrypoint.sh
